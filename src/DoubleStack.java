/*
 https://bitbucket.org/taavivesi/home3/

 On kasutatud, kui näidisena, mille põhjal on tehtud LongStackist DoubleStack.
 See on tehtud töö eelmise aasta kodutöö 3 jaoks.
  */



import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.LinkedList;

public class DoubleStack {

    private final LinkedList<Double> stack;



   public static void main (String[] argum) {

       //double test1 = interpret("1 2 -");
       //double test2 = interpret("10. 9. +");


   }

   DoubleStack() {
      this.stack = new LinkedList<Double>();
   }

    private DoubleStack(LinkedList<Double> stack){
        this.stack = stack;
    }

   @Override
   public Object clone() throws CloneNotSupportedException {
       return new DoubleStack((LinkedList<Double>) this.stack.clone());
   }

   public boolean stEmpty() {
       return this.stack.isEmpty();
   }

   public void push (double a) {
       this.stack.push(a);
   }

   public double pop() {
       if(!stack.isEmpty()){
           return this.stack.pop();
       }
       else {
           throw new EmptyStackException();
       }
   } // pop

   public void op (String s) {
       if(s.equals(" ") | s.equals("\t")){
           return;
       }
       if ("+".equals(s)) {
           this.stack.push(this.stack.pop() + this.stack.pop());
       } else if ("-".equals(s)) {
           double a = this.pop();
           double b = this.pop();
           this.stack.push(b - a);
       } else if ("*".equals(s)) {
           this.stack.push(this.stack.pop() * this.stack.pop());

       } else if ("/".equals(s)) {
           double a = this.pop();
           double b = this.pop();
           this.stack.push(b / a);
       } else {
           throw new RuntimeException("Invalid Operator: " + s + " for: " + this.stack);
       }

       if (stEmpty()){
           throw new EmptyStackException();
       }
   }


   public double tos() {
       if(!stack.isEmpty()){
           return this.stack.peek();
       }
       else{
           throw new EmptyStackException();
       }
            }

   @Override
   public boolean equals (Object o) {
       return this.stack.equals(((DoubleStack)o).stack);
        }

   @Override
   public String toString() {
       StringBuilder stringb = new StringBuilder();
       Iterator<Double> i = this.stack.descendingIterator();
       while(i.hasNext()){
           stringb.append(i.next());
           stringb.append(" ");
       }
       return stringb.toString();   }

   public static double interpret (String pol) {
       DoubleStack doublestack = new DoubleStack();
       for (int i = 0; i < pol.length(); i++) {
           char c = pol.charAt(i);
           char nc = ' ';
           if (i + 1 < pol.length()) {
               nc = pol.charAt(i + 1);
           }
           if ((c == '-' & nc >= '0' & nc <= '9') | (c >= '0' & c <= '9')) {
               StringBuilder buffer = new StringBuilder();
               buffer.append(c);
               i++;
               for (; i < pol.length(); i++) {
                   c = pol.charAt(i);
                   if (c >= '0' & c <= '9') {
                       buffer.append(c);
                   } else {
                       break;
                   }
               }
               doublestack.push(Double.parseDouble(buffer.toString()));
           } else try {
               doublestack.op("" + c);
           } catch (RuntimeException e) {
               throw new RuntimeException("Invalid Operator: " + c + " Or the operator for the polish notion is reversed: " + pol);
           }
       }
       if (doublestack.stack.size() == 1) {
           return doublestack.pop();
       } else {
           throw new RuntimeException("The following input isn't in reverse polish notion: " + pol);
       }
   }
}

